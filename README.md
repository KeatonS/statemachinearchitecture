This is a short project by Keaton Smith to demonstrate a Game architecture using the State Machine pattern and Separation of Concerns.

To make a new State:
1) Have a new animation made for the Player. Make one by selecting the player, hitting Window->animation, and then drag sprites into the animation window. Adjust samples to 12.
2) Look in the animator of the player and there should be a new state with that animation in it. Draw transitions to that state and set the conditions. (If it should transition instantly based off the condition, change Has Exit Time to false. Set transition time to zero and uncheck Fixed Duration)
3) Now that the animation is in the Animator, go to the States.cs file and create a new state of your own custom need. Make sure it inherits from some type of State. Then go to StateMachineWithStates.cs script and declare and insantiate your new State. Add the condition to call SwitchState(yourState) to your new state.
4) Test your code and notice how it enters your states!


To Play:
- XBOX controller support
- Keyboard and mouse support
	- WASD to move
	- Numberpad for all the face buttons
		- 4 to jump
		- 5 to dash
		- 7 to attack (no state coded)
		- 8 to shoot (no state coded) 

Sites mentioned in presentation:
- UI - https://www.youtube.com/watch?v=OWtQnZsSdEU 
	- Make custom onClick methods, buttons in horizontal and vertical layout groups. 
	Only problem is he doesn't use prefabs for the buttons, so if you want to change 
	all the buttons you have to go into each one individually.

 - Audio - https://www.youtube.com/watch?v=6OT43pvUyfY 
	- Shows how to separate your audio very well into an audioManager with multiple audio layers

 - Better Jumping Code - https://www.youtube.com/watch?v=7KiK0Aqtmzc 
	- Wish I saw this before writing the Jump code in our game from last year.

 - StateMachineBehaviors in Unity - https://medium.com/the-unity-developers-handbook/dont-re-invent-finite-state-machines-how-to-repurpose-unity-s-animator-7c6c421e5785 
	- The main inspiration for my state machine code. Biggest difference from mine is that he used StateMachineBehaviors, which aren't allowed to do certain things like call GameObject.GetComponent<>(), which is very important in unity. 
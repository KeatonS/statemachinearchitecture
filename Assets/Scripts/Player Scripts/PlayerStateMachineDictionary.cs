﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/*
 
     This is a slightly more complex version of a state machine. 
     This uses dictionary and animator state hashes.
     This is relatively faster than using AnimatorStateInfo.IsName()
     This differs from PlayerStateMachineWithStates as it uses the states 
        classes contained in a Dictionary with animatorStateHash as the key
     
*/
[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(PlayerStats))]
public class PlayerStateMachineDictionary : MonoBehaviour {

    private Animator playerAnim;
    private AnimatorStateInfo animStateInfo;
    private Rigidbody2D playerRB;
    private PlayerStats playerStats;

    private State curState, prevState;

    private Dictionary<int, State> statesToHash = new Dictionary<int, State>();

    void Start () {
        playerRB = GetComponent<Rigidbody2D>();
        playerAnim = GetComponent<Animator>();
        playerStats = GetComponent<PlayerStats>();

        string curLayer = "Base Layer.";
        statesToHash.Add(Animator.StringToHash(curLayer + "Idle"), new IdleState(gameObject));
        statesToHash.Add(Animator.StringToHash(curLayer + "Walk"), new WalkState(gameObject));
        statesToHash.Add(Animator.StringToHash(curLayer + "Dash"), new DashState(gameObject));
        statesToHash.Add(Animator.StringToHash(curLayer + "Jump"), new JumpState(gameObject));
        statesToHash.Add(Animator.StringToHash(curLayer + "Fall"), new FallState(gameObject));

        curState = prevState = new IdleState(gameObject);
    }

    void Update () {

        int animStateHash = playerAnim.GetCurrentAnimatorStateInfo(0).fullPathHash;

        SwitchStates(statesToHash[animStateHash]);

        curState.Execute();
    }

    private void SwitchStates(State newState)
    {
        if (newState != curState)
        {
            //Debug.Log("Old: " + curState.ToString() + " / New: " + newState.ToString());
            prevState = curState;
            curState = newState;
            prevState.Exit();
            curState.Enter();
        }
    }
}

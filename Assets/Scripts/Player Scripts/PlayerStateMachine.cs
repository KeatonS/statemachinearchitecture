﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 
     This is the simplified version of a state machine. 
     This uses AnimatorStateInfor.IsName(), which is relatively slow.
     Also avoids using state classes
     
*/
[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(PlayerStats))]
public class PlayerStateMachine : MonoBehaviour {

    private Animator playerAnim;
    private AnimatorStateInfo animStateInfo;
    private Rigidbody2D playerRB;
    private PlayerStats playerStats;

    void Start () {
        playerRB = GetComponent<Rigidbody2D>();
        playerAnim = GetComponent<Animator>();
        playerStats = GetComponent<PlayerStats>();
	}
	
	// Update is called once per frame
	void Update () {

        animStateInfo = playerAnim.GetCurrentAnimatorStateInfo(0);// Get Animator State Info on every frame

        if (animStateInfo.IsName("Idle"))
        {
            Debug.Log("Idle");
            if (playerAnim.GetBool("Grounded") && Mathf.Abs(playerRB.velocity.x) >= Mathf.Epsilon)
            {
                playerRB.velocity = Vector2.Lerp(playerRB.velocity, new Vector2(0, playerRB.velocity.y), playerStats.groundFriction * Time.fixedDeltaTime); // Friction
            }
        }
        else if (animStateInfo.IsName("Walk"))
        {
            Debug.Log("Walk");
            playerRB.velocity = new Vector2(playerStats.walkSpeed * Mathf.Ceil(Mathf.Abs(playerAnim.GetFloat("Horizontal_Input"))) * playerAnim.GetInteger("SideFacing"), playerRB.velocity.y);
        }
        else if (animStateInfo.IsName("Dash"))
        {
            Debug.Log("Dash");
            playerRB.velocity = new Vector2(playerStats.dashSpeed * playerAnim.GetInteger("SideFacing"), playerRB.velocity.y);
        }
        else if (animStateInfo.IsName("Jump"))
        {
            Debug.Log("Jump");
            if (playerAnim.GetBool("Grounded"))
            {
                playerRB.velocity = new Vector2(playerRB.velocity.x, playerStats.jumpVelocity);
                playerAnim.SetBool("Grounded", false);
            }
        }
        else if (animStateInfo.IsName("Fall"))
        {
            Debug.Log("Fall");        
        }
    }
}

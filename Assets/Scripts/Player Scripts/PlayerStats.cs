﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStats : MonoBehaviour {

    // Ground Stats
    public float walkSpeed;  
    public float dashSpeed;
    public float groundFriction;
    // Air Control Stats
    public float jumpVelocity;
    public float lowJumpMultiplier;
    public float normalJumpMultiplier;
    public float maxAirSpeedY;
    public float maxAirSpeedX;
    public float airAcceleration;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInput : MonoBehaviour {

    private Animator playerAnimator;

	// Use this for initialization
	void Start () {
        playerAnimator = GetComponent<Animator>();
	}

    void Update()
    {
        // Face Button Inputs
        playerAnimator.SetBool("Jump", Input.GetButtonDown("A_Button"));
        playerAnimator.SetBool("JumpHold", Input.GetButton("A_Button"));
        playerAnimator.SetBool("Dash", Input.GetButton("B_Button"));
        playerAnimator.SetBool("Attack", Input.GetButton("X_Button"));
        playerAnimator.SetBool("Shoot", Input.GetButton("Y_Button"));

        // Analog Stick / WASD directional inputs
        if (Input.GetAxisRaw("Horizontal_Keys") != 0) playerAnimator.SetFloat("Horizontal_Input", Input.GetAxisRaw("Horizontal_Keys")); // Keyboard Input
        else playerAnimator.SetFloat("Horizontal_Input", Input.GetAxisRaw("Horizontal_Controller_L")); // Controller Input

        if (Input.GetAxisRaw("Vertical_Keys") != 0) playerAnimator.SetFloat("Vertical_Input", Input.GetAxisRaw("Vertical_Keys")); // Keyboard Input
        else playerAnimator.SetFloat("Vertical_Input", Input.GetAxisRaw("Vertical_Controller_L")); // Controller Input

    }
}

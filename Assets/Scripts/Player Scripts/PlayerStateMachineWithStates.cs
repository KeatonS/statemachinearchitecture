﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 
     This is a slightly more complex version of a state machine. 
     This uses AnimatorStateInfor.IsName(), which is relatively slow.
     This differs from PlayerStateMachine as it uses the states classes
     
*/
[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(PlayerStats))]
public class PlayerStateMachineWithStates : MonoBehaviour
{

    private Animator playerAnim;
    private AnimatorStateInfo animStateInfo;
    private Rigidbody2D playerRB;
    private PlayerStats playerStats;

    // State objects
    private State idleState, walkState, dashState, jumpState, fallState;
    private State curState, prevState;

    void Start()
    {
        playerRB = GetComponent<Rigidbody2D>();
        playerAnim = GetComponent<Animator>();
        playerStats = GetComponent<PlayerStats>();

        idleState = new IdleState(gameObject);
        walkState = new WalkState(gameObject);
        dashState = new DashState(gameObject);
        jumpState = new JumpState(gameObject);
        fallState = new FallState(gameObject);

        curState = prevState = idleState;
    }

    void Update()
    {

        animStateInfo = playerAnim.GetCurrentAnimatorStateInfo(0);// Get Animator State Info on every frame

        if (animStateInfo.IsName("Idle"))
        {
            SwitchStates(idleState);
        }
        else if (animStateInfo.IsName("Walk"))
        {
            SwitchStates(walkState);
        }
        else if (animStateInfo.IsName("Dash"))
        {
            SwitchStates(dashState);
        }
        else if (animStateInfo.IsName("Jump"))
        {
            SwitchStates(jumpState);
        }
        else if (animStateInfo.IsName("Fall"))
        {
            SwitchStates(fallState);
        }
        else
        {
            Debug.LogError("Non existent state trying to be executed");
            SwitchStates(idleState);
        }

        curState.Execute(); // Execute the current state in the state machine
    }

    private void SwitchStates(State newState)
    {
        if (newState != curState)
        {
            //Debug.Log("Old: " + curState.ToString() + " / New: " + newState.ToString());
            prevState = curState;
            curState = newState;
            prevState.Exit();
            curState.Enter();
        }      
    }
}

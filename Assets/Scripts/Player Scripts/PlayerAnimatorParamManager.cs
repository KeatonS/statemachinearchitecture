﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimatorParamManager : MonoBehaviour {

    private Animator playerAnimator;
    private Rigidbody2D playerRB;
    //[HideInInspector]
    public bool grounded = false;
    //[HideInInspector]
    public int sideFacing = 1;
   
    void Start()
    {
        playerAnimator = GetComponent<Animator>();
        playerRB = GetComponent<Rigidbody2D> ();
        playerAnimator.SetBool("CanRotate", true);
        playerAnimator.SetInteger("SideFacing", 1);
    }

    void Update()
    {
        // Rotating player Sprite
        RotatePlayer();

        // Current Player Stats
        playerAnimator.SetBool("Grounded", grounded);
        playerAnimator.SetInteger("SideFacing", sideFacing);
        playerAnimator.SetFloat("Horizontal_Velocity", playerRB.velocity.x);
        playerAnimator.SetFloat("Vertical_Velocity", playerRB.velocity.y);       
    }

    private void RotatePlayer()
    {
        if ((int)Mathf.Sign(playerAnimator.GetFloat("Horizontal_Input")) == sideFacing * -1 && playerAnimator.GetFloat("Horizontal_Input") != 0 && playerAnimator.GetBool("CanRotate")) // Side Flipping
        {
            this.gameObject.transform.Rotate(0, 180, 0);
            sideFacing *= -1;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class State {

    protected GameObject owner;
    protected Rigidbody2D rigidbody;
    protected PlayerStats stats;
    protected Animator animator;

    public State(GameObject owner)
    {
        this.owner = owner;
        rigidbody = owner.GetComponent<Rigidbody2D>();
        stats = owner.GetComponent<PlayerStats>();
        animator = owner.GetComponent<Animator>();
    }
    public virtual void Enter() { }
    public virtual void Execute() { }
    public virtual void Exit() { }
}

public abstract class MovementState : State
{
    public MovementState(GameObject owner) : base(owner) { }

    public override void Enter()
    {
        base.Enter();
    }
    public override void Execute()
    {
        base.Execute();
    }
    public override void Exit()
    {
        base.Exit();
    }
}

public abstract class GroundMovementState : MovementState
{
    public GroundMovementState(GameObject owner) : base(owner) { }

    public override void Enter()
    {
        base.Enter();
    }
    public override void Execute()
    {
        base.Execute();
    }
    public override void Exit()
    {
        base.Exit();
    }
}

public abstract class AirMovementState : MovementState
{
    public AirMovementState(GameObject owner) : base(owner) { }

    public override void Enter()
    {
        base.Enter();
    }
    public override void Execute()
    {
        base.Execute();

        // Increase Gravity after reaching jump peak
        if (rigidbody.velocity.y > 0 && !animator.GetBool("JumpHold"))// + Y veloc, going up and no jump input
        {
            rigidbody.velocity += Vector2.up * Physics.gravity.y * (stats.lowJumpMultiplier - 1) * Time.deltaTime; // Add gravity multiplier
        }
        else if (rigidbody.velocity.y < 0 && rigidbody.velocity.y > stats.maxAirSpeedY) // - Y veloc, going down
        {
            rigidbody.velocity += Vector2.up * Physics.gravity.y * (stats.normalJumpMultiplier - 1) * Time.deltaTime; // Add gravity multiplier
        }

        // Player adjusting self in midair
        if (animator.GetFloat("Horizontal_Input") != 0f)
        {
            if (Mathf.Abs(rigidbody.velocity.x) <= stats.maxAirSpeedX || Mathf.Sign(rigidbody.velocity.x) != Mathf.Sign(animator.GetFloat("Horizontal_Input")))
            {
                rigidbody.velocity += Vector2.right * (stats.airAcceleration * animator.GetFloat("Horizontal_Input")) * Time.deltaTime;
            }
        }

        // Maintain Terminal fall speed
        if (rigidbody.velocity.y < stats.maxAirSpeedY)
        {
            rigidbody.velocity = new Vector2(rigidbody.velocity.x, stats.maxAirSpeedY);
        }
    }
    public override void Exit()
    {
        base.Exit();
    }
}

public class IdleState : GroundMovementState
{
    public IdleState(GameObject owner) : base(owner) { }

    public override void Enter()
    {
        base.Enter();
    }
    public override void Execute()
    {
        base.Execute();

        if (animator.GetBool("Grounded") && Mathf.Abs(rigidbody.velocity.x) >= Mathf.Epsilon)
        {
            rigidbody.velocity = Vector2.Lerp(rigidbody.velocity, new Vector2(0, rigidbody.velocity.y), stats.groundFriction * Time.fixedDeltaTime); // Friction
        }
    }
    public override void Exit()
    {
        base.Exit();
    }
}

public class WalkState : GroundMovementState
{
    public WalkState(GameObject owner) : base(owner) { }

    public override void Enter()
    {
        base.Enter();
    }
    public override void Execute()
    {
        base.Execute();
        rigidbody.velocity = new Vector2(stats.walkSpeed * animator.GetInteger("SideFacing"), rigidbody.velocity.y);
    }
    public override void Exit()
    {
        base.Exit();
    }
}

public class DashState : GroundMovementState
{
    public DashState(GameObject owner) : base(owner) { }

    public override void Enter()
    {
        base.Enter();
        rigidbody.gravityScale = 0f;
        //animator.SetBool("CanRotate", false);
    }
    public override void Execute()
    {
        base.Execute();
        rigidbody.velocity = new Vector2(stats.dashSpeed * animator.GetInteger("SideFacing"), 0f);
    }
    public override void Exit()
    {
        base.Exit();
        rigidbody.gravityScale = 1f;
        //animator.SetBool("CanRotate", true);
    }
}

public class JumpState : AirMovementState
{
    public JumpState(GameObject owner) : base(owner) { }

    public override void Enter()
    {
        base.Enter();
        rigidbody.velocity = new Vector2(rigidbody.velocity.x, stats.jumpVelocity);
    }
    public override void Execute()
    {
        base.Execute();
    }
    public override void Exit()
    {
        base.Exit();
    }
}

public class FallState : AirMovementState
{
    public FallState(GameObject owner) : base(owner) { }

    public override void Enter()
    {
        base.Enter();
    }
    public override void Execute()
    {
        base.Execute();
    }
    public override void Exit()
    {
        base.Exit();
    }
}